package com.tsystems.javaschool.tasks.pyramid;


import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
    	
    	new CannotBuildPyramidException();
    	int[][] pyramid;   	
		if (inputNumbers.size() > 2147483645) {			
			/**
			* @link CannotBuildPyramidException
			*/
			return pyramid = null;
		}				
		int sum = 0;
	    int numberLines = 0;
	    while (inputNumbers.size() != sum) {  	
	    	numberLines++;
	    	sum = sum + numberLines;
	    	
		    if (sum > inputNumbers.size()){
				/**
				* @link CannotBuildPyramidException
				*/
		    	return pyramid = null;
		    }
	    }	    
	    try {	
		    Collections.sort(inputNumbers);
		    }
		    catch(Exception e) {
				/**
				* @link CannotBuildPyramidException
				*/
		    	return pyramid = null;
			}	    
	    int numberRows = numberLines * 2 - 1;		    		    		    
	    int middle = numberRows / 2; 
	    int indexInput = 0;
	    pyramid = new int[numberLines][numberRows];
	    try {	
		    for(int i = 0; i < pyramid.length;  i++) {
		    	for (int j = 0; j < pyramid[0].length;  j++) {
		    		
			    	if ((j >= (middle-i)) && (j <= (middle+i))) {
			    		
			        	pyramid[i][j] = inputNumbers.get(indexInput);
			        	indexInput++;
			        	j++;
			    	}
		    	}
		    }
	    }
	    catch(Exception e) {
			/**
			* @link CannotBuildPyramidException
			*/
	    	return pyramid = null;
		}
		return pyramid;	    
		}	        
    }


