package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class Expression {

// This class has two methods: split and count
// Method split is used to separate input string into numbers and math signs
// Method count is used to count expression without brackets

	public static String count (List<String>  list) {
	try {	
		while (list.contains("*") || list.contains("/")) {			
			
			for (int i = 0; i < list.size(); i++) {
				
			if (list.get(i).equals("*") || list.get(i).equals("/")) {
				
				String count = FirstPriority.Count(list.get(i), list.get(i-1), list.get(i+1));	
				// Simplify expression after count
				List<String> listNew = new ArrayList<String>();
				for (int j = 0; j < i-1; j++) {listNew.add(list.get(j));}
				listNew.add(count);		
				for (int j = i+2; j < list.size(); j++) {listNew.add(list.get(j));}	
				list = listNew;
			}	
			}
		}
			while (list.contains("+") || list.contains("-")) {				
				
				for (int i = 0; i < list.size(); i++) {
					
				if (list.get(i).equals("+") || list.get(i).equals("-")) {
					
					String count = SecondPriority.Count(list.get(i), list.get(i-1), list.get(i+1));	
					// Simplify expression after count
					List<String> listNew = new ArrayList<String>();
					for (int j = 0; j < i-1; j++) {listNew.add(list.get(j));}
					listNew.add(count);		
					for (int j = i+2; j < list.size(); j++) {listNew.add(list.get(j));}	
					list = listNew;
				}	
				}			
			}
	return list.get(0);			
	}
	catch(Exception e) {
		return null;	
		}
	}

	public static List<String> split(String input) {
		
		List<String> list = new ArrayList<String>();
		String number = "";

		char[] listInput = input.toCharArray();					
		for (int i = 0; i < listInput.length; i++)  {			
						
			if (Character.isDigit(listInput[i]) || listInput[i] == '.') {
				number = number + listInput[i];
				if (i == listInput.length-1 || (!Character.isDigit(listInput[i+1]) && listInput[i+1] != '.')) {
					list.add(number);
					number = "";
				}
			}
			else {
				list.add(String.valueOf(listInput[i]));				
			}
			
		}				
	return list;		
	}
	public static List<String> countBrackets(List<String>  list) {
	
		String value = "";
		int indexLeftBracket = 0;
		int indexRightBracket = 0;
		List<String> bracketsExpression = new ArrayList<String>();
			for (int i = 0; i < list.size(); i++) {					
				if (list.get(i).equals("(")) {
					indexLeftBracket = i;
					break;
				}			
			}
			for (int i = list.size()-1; i > 0; i--) {					
				if (list.get(i).equals(")")) {
					indexRightBracket = i;
					break;
				}			
			}
			
			// This cycle pull out expression in brackets
			for (int j = indexLeftBracket+1; j < indexRightBracket; j++) {
				bracketsExpression.add(list.get(j));
			}		
		value = Expression.count(bracketsExpression);	
		// Simplify expression after count
		List<String> listTemp = new ArrayList<String>();		
		for (int j = 0; j < indexLeftBracket; j++) {listTemp.add(list.get(j));}		
		listTemp.add(value);		
		for (int j = indexRightBracket+1; j < list.size(); j++) {listTemp.add(list.get(j));}	
		list = listTemp;
	return list;		
	}
}
