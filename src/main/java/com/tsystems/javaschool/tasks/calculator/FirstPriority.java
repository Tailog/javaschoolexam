package com.tsystems.javaschool.tasks.calculator;

public class FirstPriority {

public static String output;	

	public static String Count (String operator, String num1String, String num2String) {
		
		float num1 =  Float.parseFloat(num1String);
		float num2 =  Float.parseFloat(num2String);
		float count = 0;
		if (num2 == 0) {
			return null;	
		}
		switch(operator){
		 case "*":
		 count = num1 * num2;
		 break;
		 case "/":
		 count = num1 / num2;
		 break;
		}
	output = String.valueOf(count);
	return output;		
	}	
}