package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
    	
    	if (statement == null || statement == "") {
    		return null;
    	}		
    	statement = statement.replaceAll("\\s+", " ");	
    	List<String> list = Expression.split(statement);
    	
    	if (list.contains(",")) {
    		return null;
    	}	
    	while (list.contains("(") && list.contains(")")) {				
    		list = Expression.countBrackets(list);
    		for (int i = 0; i < list.size(); i++) {
    			
    			String value = Expression.count(list);	
    			List<String> listTemp = new ArrayList<String>();		
    			for (int j = 0; j < i-1; j++) {listTemp.add(list.get(j));}
    			listTemp.add(value);		
    			for (int j = i+2; j < list.size(); j++) {listTemp.add(list.get(j));}	
    			list = listTemp;
    		}
    	}
    		
    	for (int i = 0; i < list.size(); i++) {
    		
    		String value = Expression.count(list);	
    		List<String> listTemp = new ArrayList<String>();		
    		for (int j = 0; j < i-1; j++) {listTemp.add(list.get(j));}
    		listTemp.add(value);		
    		for (int j = i+2; j < list.size(); j++) {listTemp.add(list.get(j));}	
    		list = listTemp;
    	}
    	if (list.get(0) == null) {
    		return null;
    	}
    	double result = Float.parseFloat(list.get(0));
    	result = Math.round(result * 10000.0) / 10000.0;
    	if (result % 1.0 != 0)
    	    return String.format("%s", result);
    	else
    	    return String.format("%.0f", result);
    }
}