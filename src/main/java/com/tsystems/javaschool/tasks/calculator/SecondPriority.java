package com.tsystems.javaschool.tasks.calculator;

public class SecondPriority {

public static String output;	

	public static String Count (String operator, String num1String, String num2String) {
		
		float num1 =  Float.parseFloat(num1String);
		float num2 =  Float.parseFloat(num2String);
		float count = 0;
		
		switch(operator){
		 case "+":
		 count = num1 + num2;
		 break;
		 case "-":
		 count = num1 - num2;
		 break;
		}
	output = String.valueOf(count);
	return output;		
	}	
}