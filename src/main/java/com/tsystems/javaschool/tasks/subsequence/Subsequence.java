package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
    
        try {
    		if (x.size() > y.size()) {
    		return false;
    		}
    	}
    	catch(Exception e) {
    		System.out.println("Wrong input");
    	}
    	int indexY = 0;
    	int foundCharacter = 0;
    	try {
    			

        	for (int indexX=0; indexX < x.size(); indexX++) {   		   		
    			while (x.get(indexX) != y.get(indexY)) {
        			indexY++;   	
        		}
    			foundCharacter++;			    		
        	}
        	if (foundCharacter == x.size()) {
        		return true;
        	} 	
            return false;	
    		
    	}
    	catch(Exception e) {
    		return false;	
    	}
    }
}
